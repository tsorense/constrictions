function printPDF(axID,figID,fnam)

set(axID,'units','centimeters')
pos = get(axID,'Position');
ti = get(axID,'TightInset');

set(figID, 'PaperUnits','centimeters');
set(figID, 'PaperSize', [pos(3)+ti(1)+ti(3) pos(4)+ti(2)+ti(4)]);
set(figID, 'PaperPositionMode', 'manual');
set(figID, 'PaperPosition',[0 0 pos(3)+ti(1)+ti(3) pos(4)+ti(2)+ti(4)]);
set(axID,'LooseInset',ti)

print(figID, '-dpdf', fnam);
end