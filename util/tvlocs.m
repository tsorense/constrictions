function varargout = tvlocs(varargin)
% TVLOCS MATLAB code for tvlocs.fig
%      TVLOCS, by itself, creates a new TVLOCS or raises the existing
%      singleton*.
%
%      H = TVLOCS returns the handle to a new TVLOCS or the handle to
%      the existing singleton*.
%
%      TVLOCS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TVLOCS.M with the given input arguments.
%
%      TVLOCS('Property','Value',...) creates a new TVLOCS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before tvlocs_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to tvlocs_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help tvlocs

% Last Modified by GUIDE v2.5 09-Mar-2016 14:03:22

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @tvlocs_OpeningFcn, ...
                   'gui_OutputFcn',  @tvlocs_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before tvlocs is made visible.
function tvlocs_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to tvlocs (see VARARGIN)

% Choose default command line output for tvlocs
handles.output = hObject;
pathOut = varargin{1}; % must have pathOut as argument
handles.pathOut = pathOut;
load(fullfile(handles.pathOut,'contourdata.mat'))
handles.subj = fields(contourdata);

set(handles.popupmenu1, 'String', handles.subj);

% Update handles structure
guidata(hObject, handles);

% % This sets up the initial plot - only do when we are invisible
% % so window can get raised using tvlocs.
% if strcmp(get(hObject,'Visible'),'off')
%     plot(rand(5));
% end

% UIWAIT makes tvlocs wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = tvlocs_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
axes(handles.axes1);
cla;

load(fullfile(handles.pathOut,'contourdata.mat'))
popup_sel_index = get(handles.popupmenu1, 'Value');
subj = get(handles.popupmenu1, 'String');

frame_index = randi(size(contourdata.(subj{popup_sel_index}).X,1));
handles.frame_index.(subj{popup_sel_index}) = frame_index;
handles.tvlocs.(subj{popup_sel_index}) = struct();
guidata(hObject,handles);

X = contourdata.(subj{popup_sel_index}).X(frame_index,:);
Y = contourdata.(subj{popup_sel_index}).Y(frame_index,:);
sectionsID = contourdata.(subj{popup_sel_index}).SectionsID;
plot_from_xy([X Y],sectionsID,'k');


% --------------------------------------------------------------------
function FileMenu_Callback(hObject, eventdata, handles)
% hObject    handle to FileMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function OpenMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to OpenMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
file = uigetfile('*.fig');
if ~isequal(file, 0)
    open(file);
end

% --------------------------------------------------------------------
function PrintMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to PrintMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
printdlg(handles.figure1)

% --------------------------------------------------------------------
function CloseMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to CloseMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
selection = questdlg(['Close ' get(handles.figure1,'Name') '?'],...
                     ['Close ' get(handles.figure1,'Name') '...'],...
                     'Yes','No','Yes');
if strcmp(selection,'No')
    return;
end

delete(handles.figure1)


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
     set(hObject,'BackgroundColor','white');
end

set(hObject, 'String', 'DUMMY');


% --- Executes on button press in pharL_button.
function pharL_button_Callback(hObject, eventdata, handles)
% hObject    handle to pharL_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
pharID = 7;

[x,y] = getpts(handles.figure1);

load(fullfile(handles.pathOut,'contourdata.mat'))
popup_sel_index = get(handles.popupmenu1, 'Value');
subj = get(handles.popupmenu1, 'String');
sectionsID = contourdata.(subj{popup_sel_index}).SectionsID;

frame_index = handles.frame_index.(subj{popup_sel_index});
X = contourdata.(subj{popup_sel_index}).X(frame_index,sectionsID==pharID);
Y = contourdata.(subj{popup_sel_index}).Y(frame_index,sectionsID==pharID);

[~,start] = min(sqrt((X-x(1)).^2+(Y-y(1)).^2));
[~,stop] = min(sqrt((X-x(2)).^2+(Y-y(2)).^2));

subj = get(handles.popupmenu1,'String');
indx = get(handles.popupmenu1,'Value');
handles.tvlocs.(subj{indx}).pharL.start = start;
handles.tvlocs.(subj{indx}).pharL.stop = stop;
handles.tvlocs.(subj{indx}).pharL.aux = 7;

guidata(hObject,handles);


% --- Executes on button press in pharU_button.
function pharU_button_Callback(hObject, eventdata, handles)
% hObject    handle to pharU_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
pharID = 7;

[x,y] = getpts(handles.figure1);

load(fullfile(handles.pathOut,'contourdata.mat'))
popup_sel_index = get(handles.popupmenu1, 'Value');
subj = get(handles.popupmenu1, 'String');
sectionsID = contourdata.(subj{popup_sel_index}).SectionsID;

frame_index = handles.frame_index.(subj{popup_sel_index});
X = contourdata.(subj{popup_sel_index}).X(frame_index,sectionsID==pharID);
Y = contourdata.(subj{popup_sel_index}).Y(frame_index,sectionsID==pharID);

[~,start] = min(sqrt((X-x(1)).^2+(Y-y(1)).^2));
[~,stop] = min(sqrt((X-x(2)).^2+(Y-y(2)).^2));

subj = get(handles.popupmenu1,'String');
indx = get(handles.popupmenu1,'Value');
handles.tvlocs.(subj{indx}).pharU.start = start;
handles.tvlocs.(subj{indx}).pharU.stop = stop;
handles.tvlocs.(subj{indx}).pharU.aux = 7;

guidata(hObject,handles);

% --- Executes on button press in alv_button.
function alv_button_Callback(hObject, eventdata, handles)
% hObject    handle to alv_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
alvID = 11;

[x,y] = getpts(handles.figure1);

load(fullfile(handles.pathOut,'contourdata.mat'))
popup_sel_index = get(handles.popupmenu1, 'Value');
subj = get(handles.popupmenu1, 'String');
sectionsID = contourdata.(subj{popup_sel_index}).SectionsID;

frame_index = handles.frame_index.(subj{popup_sel_index});
X = contourdata.(subj{popup_sel_index}).X(frame_index,sectionsID==alvID);
Y = contourdata.(subj{popup_sel_index}).Y(frame_index,sectionsID==alvID);

[~,start] = min(sqrt((X-x(1)).^2+(Y-y(1)).^2));
[~,stop] = min(sqrt((X-x(2)).^2+(Y-y(2)).^2));

subj = get(handles.popupmenu1,'String');
indx = get(handles.popupmenu1,'Value');
handles.tvlocs.(subj{indx}).alv.start = start;
handles.tvlocs.(subj{indx}).alv.stop = stop;
handles.tvlocs.(subj{indx}).alv.aux = 11;

guidata(hObject,handles);

% --- Executes on button press in pal_button.
function pal_button_Callback(hObject, eventdata, handles)
% hObject    handle to pal_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
palID = 11;

[x,y] = getpts(handles.figure1);

load(fullfile(handles.pathOut,'contourdata.mat'))
popup_sel_index = get(handles.popupmenu1, 'Value');
subj = get(handles.popupmenu1, 'String');
sectionsID = contourdata.(subj{popup_sel_index}).SectionsID;

frame_index = handles.frame_index.(subj{popup_sel_index});
X = contourdata.(subj{popup_sel_index}).X(frame_index,sectionsID==palID);
Y = contourdata.(subj{popup_sel_index}).Y(frame_index,sectionsID==palID);

[~,start] = min(sqrt((X-x(1)).^2+(Y-y(1)).^2));
[~,stop] = min(sqrt((X-x(2)).^2+(Y-y(2)).^2));

subj = get(handles.popupmenu1,'String');
indx = get(handles.popupmenu1,'Value');
handles.tvlocs.(subj{indx}).pal.start = start;
handles.tvlocs.(subj{indx}).pal.stop = stop;
handles.tvlocs.(subj{indx}).pal.aux = 11;

guidata(hObject,handles);

% --- Executes on button press in softpal_button.
function softpal_button_Callback(hObject, eventdata, handles)
% hObject    handle to softpal_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
velID = 12;

[x,y] = getpts(handles.figure1);

load(fullfile(handles.pathOut,'contourdata.mat'))
popup_sel_index = get(handles.popupmenu1, 'Value');
subj = get(handles.popupmenu1, 'String');
sectionsID = contourdata.(subj{popup_sel_index}).SectionsID;

frame_index = handles.frame_index.(subj{popup_sel_index});
X = contourdata.(subj{popup_sel_index}).X(frame_index,sectionsID==velID);
Y = contourdata.(subj{popup_sel_index}).Y(frame_index,sectionsID==velID);

[~,start] = min(sqrt((X-x(1)).^2+(Y-y(1)).^2));
[~,stop] = min(sqrt((X-x(2)).^2+(Y-y(2)).^2));

subj = get(handles.popupmenu1,'String');
indx = get(handles.popupmenu1,'Value');
handles.tvlocs.(subj{indx}).softpal.start = start;
handles.tvlocs.(subj{indx}).softpal.stop = stop;
handles.tvlocs.(subj{indx}).softpal.aux = 12;

guidata(hObject,handles);


% --- Executes on button press in save_button.
function save_button_Callback(hObject, eventdata, handles)
% hObject    handle to save_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
tvlocs = handles.tvlocs;
save(fullfile(handles.pathOut,'tvlocs.mat'), 'tvlocs')


% --- Executes on button press in print_button.
function print_button_Callback(hObject, eventdata, handles)
% hObject    handle to print_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
axID = handles.axes1;
figID = handles.figure1;
fnam = 'test.pdf';
printPDF(axID,figID,fnam)
