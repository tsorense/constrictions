function [D,x1,y1,x2,y2] = getCD(Xout,Yout,Xin,Yin)
% Compute constriction between out and in articulators.

    [D, xc_is, yc_is] = poly_poly_dist(Xout,Yout,Xin,Yin);
    
    x1 = xc_is(1);
    y1 = yc_is(1);
    x2 = xc_is(2);
    y2 = yc_is(2);
end
