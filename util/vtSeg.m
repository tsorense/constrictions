function [X,Y,Xul,Yul,Xll,Yll,Xtongue,Ytongue,Xalv,Yalv,Xpal,Ypal,...
    Xvelum,Yvelum,Xvelar,Yvelar,XpharL,YpharL,XpharU,YpharU] ...
    = vtSeg(contourdata,fileNumber,frameNumber,tvlocs)
    
    ii = contourdata.File==fileNumber & contourdata.Frames==frameNumber;
    xy_data = [contourdata.X(ii,:),contourdata.Y(ii,:)];
    X = xy_data(1:length(xy_data)/2);
    Y = xy_data(length(xy_data)/2+1:end);
    
    % Lips
    Xul = X(ismember(contourdata.SectionsID,15));
    Yul = Y(ismember(contourdata.SectionsID,15));
    Xll = X(ismember(contourdata.SectionsID,4));
    Yll = Y(ismember(contourdata.SectionsID,4));
    
    % Tongue
    Xtongue = X(ismember(contourdata.SectionsID,2));
    Ytongue = Y(ismember(contourdata.SectionsID,2));
    
    % Palate
    Xalv = X(ismember(contourdata.SectionsID,11));
    Yalv = Y(ismember(contourdata.SectionsID,11));
    Xpal =  X(ismember(contourdata.SectionsID,11));
    Ypal =  Y(ismember(contourdata.SectionsID,11));
    Xvelum = X(ismember(contourdata.SectionsID,12));
    Yvelum = Y(ismember(contourdata.SectionsID,12));
    Xphar = X(ismember(contourdata.SectionsID,7));
    Yphar = Y(ismember(contourdata.SectionsID,7));
    
    % trim...
    ii = sort([tvlocs.alv.start tvlocs.alv.stop]);
    ii = ii(1):ii(2);
    Xalv = Xalv(ii); 
    Yalv = Yalv(ii);
    
    ii = sort([tvlocs.pal.start tvlocs.pal.stop]);
    ii = ii(1):ii(2);
    Xpal = Xpal(ii);
    Ypal = Ypal(ii);
    
    ii = sort([tvlocs.softpal.start tvlocs.softpal.stop]);
    ii = ii(1):ii(2);
    Xvelar = Xvelum(ii);
    Yvelar = Yvelum(ii);
    
    ii = sort([tvlocs.pharL.start tvlocs.pharL.stop]);
    ii = ii(1):ii(2);
    XpharL = Xphar(ii); 
    YpharL = Yphar(ii);
    
    ii = sort([tvlocs.pharU.start tvlocs.pharU.stop]);
    ii = ii(1):ii(2);
    XpharU = Xphar(ii);
    YpharU = Yphar(ii);
end