function [LA,x1,y1,x2,y2] = getLA(Xul,Yul,Xll,Yll,spatRes)
% Compute lip aperture.

    [D, xc_is, yc_is] = poly_poly_dist(Xul,Yul,Xll,Yll);
    
    x1 = xc_is(1);
    y1 = yc_is(1);
    x2 = xc_is(2);
    y2 = yc_is(2);
    LA = spatRes.*D;
end