function plotTV(datPath,graphicsPath,spatRes)
% PLOTTV - plot figure illustrating the task variable measurements

% Load contours, weights, and model.
load(fullfile(datPath, 'contourdata.mat'))
load(fullfile(datPath, 'tv.mat'))
load(fullfile(datPath, 'tvlocs.mat'))

% list of subjects
subj = fields(contourdata);
nDir = length(subj);

for h=1:nDir
    fileNumber = 1;
    frames = unique(contourdata.(subj{h}).Frames(contourdata.(subj{h}).File==fileNumber));
    frameNumber = randi(length(frames),1);
    
    X = contourdata.(subj{h}).X(frameNumber,:);
    Y = contourdata.(subj{h}).Y(frameNumber,:);
     
    figID = figure('Color','w');

    % whole vocal tract shape
    plot_from_xy([X, Y],contourdata.(subj{h}).SectionsID,[0.6 0.6 0.6])
    axis tight, hold on

    % constriction location points
    cl = fields(tvlocs.(subj{h}));
    SectionsID = contourdata.(subj{h}).SectionsID;
    for ell=1:length(fields(tvlocs.(subj{h})))+2
        
        % plot constriction locations (but not the lips or velopharyngeal
        % port)
        if ell>2
            cl_id = tvlocs.(subj{h}).(cl{ell-2});
            SectionsID_cl = cl_id.aux;
            sections_match = ismember(SectionsID, SectionsID_cl);
            nodes_match = tvlocs.(subj{h}).(cl{ell-2}).start:tvlocs.(subj{h}).(cl{ell-2}).stop;
            Xcl = X(sections_match);
            Xcl = Xcl(nodes_match);
            Ycl = Y(sections_match);
            Ycl = Ycl(nodes_match);
            plot(Xcl,Ycl,'-k','LineWidth',2)
        end
        
        % plot the constriction degrees
        Xcl = [tv.(subj{h}).tv{ell}.in(frameNumber,1), ...
            tv.(subj{h}).tv{ell}.out(frameNumber,1)];
        Ycl = [tv.(subj{h}).tv{ell}.in(frameNumber,2), ...
            tv.(subj{h}).tv{ell}.out(frameNumber,2)];
        scatter(Xcl,Ycl,50,'k','filled')
        plot(Xcl,Ycl,'-k','LineWidth',2)
    end
    hold off, axis off
    
    fnam=[graphicsPath subj{h} '_tv.png'];
    print(fnam,'-dpng')
    close(figID)
end
    
end
