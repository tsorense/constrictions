function discover_cl(pathIn,pathOut,q,ncl,spatRes)


% load constriction degree and location
load(fullfile(pathOut,'contourdata.mat'));

% constant articulator identifiers
hard_palate = 11;
soft_palate = 12;
pharynx = 7;
tongue = 2;

% constriction location names
% (consecutive letters of alphabet)
cl_names = cell(1,sum(ncl));
for i = 0:sum(ncl)-1
    cl_names{i+1} = char(65+i);
end

% subject list
subj = fields(contourdata);
nsubj = length(subj);

for i = 1:nsubj
    fprintf('Discovering constriction locations for %s\n',subj{i})
    
    % Section identifiers for each node.
    SectionsID = contourdata.(subj{i}).SectionsID;
    
    % Identify the articulators of interest by index.
    hard_palate_id = SectionsID == hard_palate;
    soft_palate_id = SectionsID == soft_palate;
    pharynx_id = SectionsID == pharynx;
    tongue_id = SectionsID == tongue;
    
    % Set hard palate and pharynx to mean; allow soft palate and tongue to
    % vary.
    X_hard_palate = mean(contourdata.(subj{i}).X(:,hard_palate_id),1);
    Y_hard_palate = mean(contourdata.(subj{i}).Y(:,hard_palate_id),1);
    X_pharynx = mean(contourdata.(subj{i}).X(:,pharynx_id),1);
    Y_pharynx = mean(contourdata.(subj{i}).Y(:,pharynx_id),1);
    X_soft_palate = contourdata.(subj{i}).X(:,soft_palate_id);
    Y_soft_palate = contourdata.(subj{i}).Y(:,soft_palate_id);
    X_tongue = contourdata.(subj{i}).X(:,tongue_id);
    Y_tongue = contourdata.(subj{i}).Y(:,tongue_id);
    
    % Separate larynx from pharynx.
    plot(X_pharynx,Y_pharynx,'LineWidth',2), axis equal
    disp('Click on the superior edge of the larynx.'), [~,Y_larynx] = ginput(1);
    close all
    
    pharynx_remove = Y_pharynx < Y_larynx;
    X_hypopharynx = X_pharynx(~pharynx_remove);
    Y_hypopharynx = Y_pharynx(~pharynx_remove);
    
    % initialize containers
    nframes = size(contourdata.(subj{i}).X,1);
    indx = zeros(nframes,1);
    cl_section = zeros(nframes,1);
    D = zeros(nframes,1);
    
    % Loop through each video frame.
    fprintf('[')
    twentieths = round(linspace(1,nframes,20));
    for j = 1:nframes
        if ismember(j,twentieths)
            fprintf('=')
        end
        
        % Compute minimum constriction at the hard palate.
        [D_hp, ~, ~, indx_hp] = poly_poly_dist(X_tongue(j,:),Y_tongue(j,:),X_hard_palate,Y_hard_palate);
        
        % Compute minimum constriction at the soft palate.
        [D_sp, ~, ~, indx_sp] = poly_poly_dist(X_tongue(j,:),Y_tongue(j,:),X_soft_palate(j,:),Y_soft_palate(j,:));
        
        % Compute minimum constriction at the pharynx.
        [D_ph, ~, ~, indx_ph] = poly_poly_dist(X_tongue(j,:),Y_tongue(j,:),X_hypopharynx,Y_hypopharynx);
        
        % Identify whether (1) hard palate, (2) soft palate, or (3)
        % hypopharynx has the minimum constriction.
        [~, ii] = min([D_hp D_sp D_ph]);
        
        % Save results to containers.
        if ii==1
            indx(j) = indx_hp(2);
            D(j) = D_hp;
        elseif ii==2
            indx(j) = indx_sp(2);
            D(j) = D_sp;
        elseif ii==3
            indx(j) = indx_ph(2);
            D(j) = D_ph;
        end
        cl_section(j) = ii;
    end
    fprintf(']\n')
    
    % Count the number of minima at each hard palate, soft palate, and
    % pharynx node.
    nNodes_hp = size(X_hard_palate,2);
    nNodes_sp = size(X_soft_palate,2);
    nNodes_ph = size(X_hypopharynx,2);
    count_hp = zeros(nNodes_hp,1);
    count_sp = zeros(nNodes_sp,1);
    count_ph = zeros(nNodes_ph,1);
    cutoff_hp = quantile(D(cl_section==1),q);
    cutoff_sp = quantile(D(cl_section==2),q);
    cutoff_ph = quantile(D(cl_section==3),q);
    for k=1:nNodes_hp
        count_hp(k) = sum(indx==k & cl_section==1 & D<cutoff_hp);
    end
    for k=1:nNodes_sp
        count_sp(k) = sum(indx==k & cl_section==2 & D<cutoff_sp);
    end
    for k=1:nNodes_ph
        count_ph(k) = sum(indx==k & cl_section==3 & D<cutoff_ph);
    end
    
    % Cluster the hard and soft palate constriction locations.
    indx_palate = [indx(cl_section==1 & D<cutoff_hp); indx(cl_section==2 & D<cutoff_sp)+nNodes_hp];
    [center_id,palate_centers] = kmeans(indx_palate, ncl(1));
    
    % Obtain constriction locations.
    % centers(:,1) - start index
    % centers(:,2) - end index
    % centers(:,3) - ID of section which first two columns index
    centers = NaN(sum(ncl),4);
    for j=1:ncl(1)
        center_size = std(indx_palate(center_id==j));
        centers(j,1:2) = round([-center_size, center_size] + palate_centers(j));
        if centers(j,1) < 1
            centers(j,1) = 1;
        end
        if centers(j,2) > nNodes_hp+nNodes_sp
            centers(j,2) = nNodes_hp+nNodes_sp;
        end
        centers(j,3:4) = [hard_palate, soft_palate];
    end
    
    % Cluster the hypopharynx constriction locations.
    indx_ph = indx(cl_section==3 & D<cutoff_ph);
    [center_id,pharynx_centers] = kmeans(indx_ph, ncl(2));
    
    % Obtain sizes for the clusters.
    for j=ncl(1)+1:sum(ncl)
        center_size = std(indx_ph(center_id==j-ncl(1)));
        centers(j,1:2) = round([-center_size, center_size] + pharynx_centers(j-ncl(1)) + find(pharynx_remove,1,'last'));
        centers(j,3) = pharynx; % fourth column entry is NaN
    end
    
    % Save constriction location centers to container.
    for j=1:sum(ncl)
        tvlocs.(subj{i}).(cl_names{j}).start = centers(j,1);
        tvlocs.(subj{i}).(cl_names{j}).stop = centers(j,2);
        tvlocs.(subj{i}).(cl_names{j}).aux = centers(j,3:end);
    end

   % Load exemplary video frame.
   aviList = dir(fullfile(pathIn,subj{i},'*.avi'));
   aviList = {aviList.name};
   aviFile = aviList{randi(length(aviList),1)};
   video = VideoReader(fullfile(pathIn,subj{i},aviFile));
   video.CurrentTime = 1.75;
   frameNo = 100;
   frame = readFrame(video);
   horizOffset = video.Width/2;
   vertOffset = video.Height/2;
   
   % Plot palate and constriction locations over a video frame for
   % visualization purposes.
   figure
   imagesc(frame), hold on
   colormap gray
   scatter(X_hard_palate./spatRes + horizOffset, ...
       -Y_hard_palate./spatRes + vertOffset, ...
       80 * count_hp/(max([count_hp; count_sp; count_ph])) + 0.5, ...
       'MarkerFaceColor', 'r', 'MarkerEdgeColor', 'w', 'LineWidth', 1)
   scatter(X_soft_palate(frameNo,:)./spatRes + horizOffset, ...
       -Y_soft_palate(frameNo,:)./spatRes + vertOffset, ...
       80 * count_sp/(max([count_hp; count_sp; count_ph])) + 0.5, ...
       'MarkerFaceColor', 'r', 'MarkerEdgeColor', 'w', 'LineWidth', 1)
   scatter(X_hypopharynx./spatRes + horizOffset, ...
       -Y_hypopharynx./spatRes + vertOffset, ...
       80 * count_ph/(max([count_hp; count_sp; count_ph])) + 0.5, ...
       'MarkerFaceColor', 'r', 'MarkerEdgeColor', 'w', 'LineWidth', 1)
   axis equal, axis off, hold off
  print([subj{i} '_1.jpeg'],'-djpeg')
  close all
  
   % Plot estimated constriction locations for visualization purposes.
   figure
   imagesc(frame), hold on
   colormap gray
   X_palate = [X_hard_palate,mean(X_soft_palate,1)];
   Y_palate = [Y_hard_palate,mean(Y_soft_palate,1)];
   scatter(X_palate(round(palate_centers))./spatRes + horizOffset, ...
       -Y_palate(round(palate_centers))./spatRes + vertOffset, 100, ...
       'MarkerFaceColor', 'r', 'MarkerEdgeColor', 'w', 'LineWidth', 1)
   scatter(X_hypopharynx(round(pharynx_centers))./spatRes + horizOffset, ...
       -Y_hypopharynx(round(pharynx_centers))./spatRes + vertOffset, 100, ...
       'MarkerFaceColor', 'r', 'MarkerEdgeColor', 'w', 'LineWidth', 1)
   axis equal, axis off, hold off
  print([subj{i} '_2.jpeg'],'-djpeg')
  close all
end

save(fullfile(pathOut,'tvlocs.mat'),'tvlocs')
