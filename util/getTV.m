function getTV(pathOut)
% GETTV - compute task variables from articulator weights
% 
% input:
%  PATHOUT - path to save output
% 
% Based on the script of Vikram Ramanarayanan (SPAN-USC, 2009).
%
% Note that constriction degrees are measured in mm.
%
% Last Updated: Oct. 13, 2016
% 
% Tanner Sorensen
% Signal Analysis and Interpretation Laboratory
% University of Southern California

% Load data of contours, weights, and model.
load(fullfile(pathOut,'contourdata.mat'))
folders = fields(contourdata);
nDir = length(folders);

% Load constriction locations.
load(fullfile(pathOut,'tvlocs.mat'))

for h = 1:nDir
    fprintf('Measuring task variables for subject %s\n',folders{h})
    
    SectionsID = contourdata.(folders{h}).SectionsID;
    cl = fields(tvlocs.(folders{h}));
    files = unique(contourdata.(folders{h}).File);
    nFile = length(files);
    
    D = zeros(100000,length(fields(tvlocs.(folders{h})))+1); x1 = D; x2 = D; y1 = D; y2 = D;
    
    k=1;
    
    fprintf('[')
    twentieths = round(linspace(1,nFile,20));
    for i=1:nFile
        if ismember(i,twentieths)
            fprintf('=')
        end
        frames = contourdata.(folders{h}).Frames(contourdata.(folders{h}).File == files(i))';
        nFrames = length(frames);
        for j=1:nFrames
            % tongue contour
            Xtng = contourdata.(folders{h}).X(j,SectionsID==2);
            Ytng = contourdata.(folders{h}).Y(j,SectionsID==2);
            Xul = contourdata.(folders{h}).X(j,SectionsID==15);
            Yul = contourdata.(folders{h}).Y(j,SectionsID==15);
            Xll = contourdata.(folders{h}).X(j,SectionsID==4);
            Yll = contourdata.(folders{h}).Y(j,SectionsID==4);
            Xvelum = contourdata.(folders{h}).X(j,SectionsID==12);
            Yvelum = contourdata.(folders{h}).Y(j,SectionsID==12);
            Xphar = contourdata.(folders{h}).X(j,SectionsID==7);
            Yphar = contourdata.(folders{h}).Y(j,SectionsID==7);
            
            [D(k,1),x1(k,1),y1(k,1),x2(k,1),y2(k,1)] = getCD(Xul,Yul,Xll,Yll);
%             [D(k,2),x1(k,2),y1(k,2),x2(k,2),y2(k,2)] = getCD(Xvelum,Yvelum,Xphar,Yphar);
            for ell=1:length(fields(tvlocs.(folders{h})))
                if strcmp(cl{ell},'pharU') % velum against nasopharynx (i.e., 'pharU')
                    sections_match = ismember(SectionsID, tvlocs.(folders{h}).(cl{ell}).aux);
                    nodes_match = sort([tvlocs.(folders{h}).(cl{ell}).start,...
                        tvlocs.(folders{h}).(cl{ell}).stop]);
                    nodes_match = nodes_match(1):nodes_match(2);
                    Xcl = contourdata.(folders{h}).X(j,sections_match);
                    Xcl = Xcl(nodes_match);
                    Ycl = contourdata.(folders{h}).Y(j,sections_match);
                    Ycl = Ycl(nodes_match);
                    [D(k,ell+1),x1(k,ell+1),y1(k,ell+1),x2(k,ell+1),y2(k,ell+1)] = getCD(Xcl,Ycl,Xvelum,Yvelum);
                else % tongue against pharynx or palate
                    sections_match = ismember(SectionsID, tvlocs.(folders{h}).(cl{ell}).aux);
                    nodes_match = sort([tvlocs.(folders{h}).(cl{ell}).start,...
                        tvlocs.(folders{h}).(cl{ell}).stop]);
                    nodes_match = nodes_match(1):nodes_match(2);
                    Xcl = contourdata.(folders{h}).X(j,sections_match);
                    Xcl = Xcl(nodes_match);
                    Ycl = contourdata.(folders{h}).Y(j,sections_match);
                    Ycl = Ycl(nodes_match);
                    [D(k,ell+1),x1(k,ell+1),y1(k,ell+1),x2(k,ell+1),y2(k,ell+1)] = getCD(Xcl,Ycl,Xtng,Ytng);
                end
            end
            
            k=k+1;
        end
    end
    fprintf(']\n')
    
    D(k:end,:)=[]; x1(k:end,:)=[]; y1(k:end,:)=[]; x2(k:end,:)=[]; y2(k:end,:)=[];
    
    for i=1:size(D,2)
        tv.(folders{h}).tv{i}.cd=D(:,i);
        tv.(folders{h}).tv{i}.in=[x1(:,i) y1(:,i)]; 
        tv.(folders{h}).tv{i}.out=[x2(:,i) y2(:,i)];
    end
end

save(fullfile(pathOut, 'tv.mat'),'tv')

end