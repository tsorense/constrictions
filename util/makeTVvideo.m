function makeTVvideo(datPath,subj,spatRes)

% PLOTTV - plot figure illustrating the task variable measurements

% Load data of contours, weights, and model.
load([datPath '/contourdata_tv.mat'])
nDir = length(contourdata_tv);
rng(9991)

load tvlocs.mat

for h=1:nDir    
    files = unique(contourdata_tv{h}.File);
    fileNumber = 1;
    frames = unique(contourdata_tv{h}.Frames(contourdata_tv{h}.File==fileNumber));
    
    % create video
    fnam=[datPath '/graphics/' subj{h} '_tv_video.avi'];
    v = VideoWriter(fnam);
    open(v)
    
    figID = figure;
    hold on
    for i=1:length(frames)
        frameNumber = frames(i);

        [X,Y,Xul,Yul,Xll,Yll,Xtongue,Ytongue,Xalv,Yalv,Xpal,Ypal,Xvelum,Yvelum,Xvelar,Yvelar,XpharL,YpharL,XpharU,YpharU] ...
            = vtSeg(contourdata_tv{h},files(fileNumber),frames(frameNumber),contourdata_tv{h}.mean_vtshape,contourdata_tv{h}.U_gfa,tvlocs.(subj{h}));
        
        % whole vocal tract shape
        plot_from_xy([X, Y],contourdata_tv{h}.SectionsID,[0.6 0.6 0.6]), hold on

        % constriction location segments
        plot(Xll, Yll, 'k', 'LineWidth', 5);
        plot(Xul, Yul, 'k', 'LineWidth', 5);

        % hard structures
        plot(XpharU, YpharU, 'k', 'LineWidth', 5);
        plot(XpharL, YpharL, 'k', 'LineWidth', 5);
        plot(Xvelar,Yvelar, 'k', 'LineWidth', 5);
        plot(Xpal,Ypal, 'k', 'LineWidth', 5);
        plot(Xalv,Yalv, 'k', 'LineWidth', 5);

        % constriction location points
        [LA,ulx,uly,llx,lly] = getLA(Xul,Yul,Xll,Yll,spatRes);
        [VEL,velumx1,velumy1,pharynxx1,pharynxy1] = getVEL(Xvelum,Yvelum,XpharU,YpharU,spatRes);
        [alveolarCD,alveolarx,alveolary,tonguex2,tonguey2] = getAlveolarCD(Xalv,Yalv,Xtongue,Ytongue,spatRes);
        [palatalCD,palatalx,palataly,tonguex3,tonguey3] = getPalatalCD(Xpal,Ypal,Xtongue,Ytongue,spatRes);
        [velarCD,velumx2,velumy2,tonguex1,tonguey1] = getVelarCD(Xvelar,Yvelar,Xtongue,Ytongue,spatRes);
        [pharyngealCD,pharynxx2,pharynxy2,tonguex4,tonguey4] = getPharyngealCD(XpharL,YpharL,Xtongue,Ytongue,spatRes);

        Xcl=[ulx llx velumx1 alveolarx palatalx velumx2 pharynxx1 ...
            tonguex1 tonguex2 tonguex3 tonguex4 pharynxx2];
        Ycl=[uly lly velumy1 alveolary palataly velumy2 pharynxy1 ...
            tonguey1 tonguey2 tonguey3 tonguey4 pharynxy2];
        scatter(Xcl,Ycl,60,[0.6 0.6 0.6],'filled')
        
        text(-30,30,subj{h})
        axis tight; axis equal; axis off;
        hold off, xlim([-40 40]), ylim([-40 40])
        
        frame = getframe(figID,[0 0 560 420]);
        writeVideo(v,frame)
        clf
    end
    close(v)
end

end