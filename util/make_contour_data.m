function make_contour_data(pathIn,pathOut)
% MAKE_CONTOUR_DATA - make all the track files in PATHIN into a matlab
% structured array CONTOURDATA saved to file CONTOURDATA_ORIGINAL.mat in
% PATHOUT. PATHIN is the parent of the directories whose names are in the 
% SUBJ cell array.
%
% input:
%   PATHIN - path to track files
%   PATHOUT - path to save output 
% 
% Created: Dec., 2015
% Last Updated: Dec. 8, 2016
% 
% Tanner Sorensen
% Signal Analysis and Interpretation Laboratory
% University of Southern California

fileFormat = '*.mat';  % file name pattern
folders = dir(pathIn);
folders = folders(arrayfun(@(x) x.name(1), folders) ~= '.');
folders = {folders.name};
nDir = length(folders);   % number of different speakers, sessions, etc.

for h=1:nDir
    fprintf('Making contourdata for %s\n', folders{h})
    
    fileList = dir(fullfile(pathIn,folders{h},fileFormat));
    fileList = {fileList.name};
    nFile = length(fileList);
    
    ell=1;
    fprintf('[')
    twentieths = round(linspace(1,nFile,20));
    for i=1:nFile
        if ismember(i,twentieths)
            fprintf('=')
        end
        
        file = load(fullfile(pathIn,folders{h},fileList{i}));
        nFrame = length(file.trackdata);

        for j=1:nFrame
            
            segment = file.trackdata{j}.contours.segment;

            segmentStart = 0;
            sectionsID = [];
            y = [];
            for k=1:(size(segment,2)-1)
                sectionsID   = cat(1,sectionsID,segmentStart+segment{k}.i);
                segmentStart = segmentStart+max(segment{k}.i);
                v            = segment{k}.v;
                y            = cat(1,y,[v(:,1),v(:,2)]);
            end
            
            if i==1 && j==1
                lenInit = 100000;
                frames = zeros(lenInit,1);
                files = zeros(lenInit,1);
                X=NaN(lenInit,size(y,1));
                Y=NaN(lenInit,size(y,1));
            else
                X(ell,:) = y(:,1)';
                Y(ell,:) = y(:,2)';
                frames(ell) = j;
                files(ell) = i;
            end
            
            ell=ell+1;
        end
    end
    fprintf(']\n')
    
    ii=isnan(X(:,1));
    X(ii,:)=[];
    Y(ii,:)=[];
    files(ii)=[];
    frames(ii)=[];
    contourdata.(folders{h}) = struct('X',X,'Y',Y,...
        'File',files,'fl',{fileList},...
        'SectionsID',sectionsID','Frames',frames);
end

save(fullfile(pathOut,'contourdata'),'contourdata')

end