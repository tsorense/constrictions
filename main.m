addpath(genpath('util'))
datPath = fullfile(cd,'data');
outPath = fullfile(cd,'mat');
graphicsPath = fullfile(cd,'graphics');

subj = {'ata','atb'};

FOV = 200; % 200 mm^2 field of view 
Npix = 68; % 68^2 total pixels
spatRes = FOV/Npix; % spatial resolution

% generate file contourdata_original
make_contour_data(datPath,outPath)

% constriction location input
tvlocs(outPath)

% measure task variables
getTV(outPath)

% plot task variable figure
plotTV(outPath,graphicsPath)
